import React from 'react'
import styled from 'styled-components';

import  {  Button } from "./Button";

import TV from '../resources/images/tab-tv.png';
import Tablet from '../resources/images/tab-tablet.png';
import Macbook from '../resources/images/tab-macbook.png';

import { generateMedia} from 'styled-media-query';


function TabContentTwo() {
    return (
       
            <TabContentContainer>
            <div className="container">
                <div className="tab-content">
                    
                    <span style={{fontSize: '1.5rem'}}>Watch TV shows and Movies anytime, anywhere - personalized for you.</span>
                    <div className="btn-container">
                        <Button className="btn">try it now</Button>
                    </div>
                </div>
                <div className="tab-bottom-content">
                    <div>
                        <img src={TV} alt="television" style={{width: '18.75rem'}} />
                        <h3>Watch on your TV</h3>
                        <p>Smart TVs, Playstation, Xbox, Chromecast, Apple TV, Blu-ray players and more.</p>
                    </div>
                    <div>
                        <img src={Tablet} alt="Mobiles" style={{width: '18.75rem', paddingTop: "0.625rem", paddingBottom: "0.625rem"}}/>
                        <h3>Watch Instantly or download for later</h3>
                        <p>Available on phone and tablet, wherever you go.</p>
                    </div>
                    <div>
                        <img src={Macbook} alt="Apple Macbooks" style={{width: '18.75rem', paddingTop: "0.625rem", paddingBottom: "0.625rem"}}/>
                        <h3>Use any computer</h3>
                        <p>Watch right on Netflix.com</p>
                    </div>
                </div>
            </div>
        </TabContentContainer>
        
    )
}

export default TabContentTwo;

const mediaQuery = generateMedia({
    smDesktop: '1440px',
    tablet: '900px'
})

const TabContentContainer = styled.div`
    background: var(--main-deep-dark);
    
    .container {
        margin: 0 15%;
    }

    img {
        width: 31.875rem;
    }

    .tab-content {
        display: grid;
        grid-template-columns: repeat(12, 1fr);
        grid-gap: 2rem;
        align-items: center;
        font-size: 2rem;
        padding: 2.5rem 0;

        ${mediaQuery.lessThan('smDesktop')`
           grid-template-columns: repeat(2, 1fr)
        `}

        ${mediaQuery.lessThan('tablet')`
            grid-template-columns: 1fr;
            text-align: center;
            row-gap: 1.5rem;
       `}    
    }

    span {
       grid-column: 1 / 8;

       ${mediaQuery.lessThan('tablet')`
            grid-column: 1 / -1;
            font-size: 1.5rem;
       `}
    }
    
    .btn {
       
       
    }
    .btn-container {
        grid-column: 9 / 12;
        margin: 0 1.25rem 1.25rem;
        ${mediaQuery.lessThan('tablet')`
            grid-column: 1 / -1;
            margin-left: 30%;
            margin-right: 30%;
       `}
    }

    .tab-bottom-content {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 2rem;
        text-align: center;
        margin-top: 2rem;
        
        ${mediaQuery.lessThan('tablet')`
            grid-template-columns: 1fr;
            
       `}    

    }

    h3 {
        margin: 0.5rem 0;
    }

    p {
        color: var(--main-grey);
    }


`