import React, { Component } from 'react'
import { Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import TabDoor from "./tab_navs/TabDoor";
import TabPrices from "./tab_navs/TabPrices";
import TabDevices from "./tab_navs/TabDevices";
import TabOne from "./TabContentOne";
import TabTwo from "./TabContentTwo";
import TabThree from "./TabContentThree"
import "../css/TabsNav.css"

class TabComponent extends Component {
    state = {
        tabIndex: 0
    }

    render() {
        return (
            <div>
                <Tabs className="tabs" selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                    <TabList className="tab-nav-container">
                        <Tab className={`${this.state.tabIndex === 0 ? 'active tab-selected' : null}`}>
                            <TabDoor />
                            <p className="lgScreen" style={{marginBottom: "1.875rem"}}><strong>No commitments<br/>Cancel online at anytime</strong></p>
                            <br/>
                            <span className="mdScreen" style={{ marginTop: '-5.3125rem'}}>Cancel</span>
                        </Tab>
                        <Tab className={`${this.state.tabIndex === 1 ? 'active tab-selected' : null}`}>
                            <TabDevices />
                        <p className="lgScreen" style={{marginTop: "-5.3125rem"}}><strong>Watch anywhere</strong></p>
                            <p className="mdScreen" style={{ marginTop: '-5.3125rem'}}>Devices</p>
                        </Tab>
                        <Tab className={`${this.state.tabIndex === 2 ? 'active tab-selected' : null}`}>
                            <TabPrices />
                            <p className="lgScreen" ><strong>Pick your price</strong></p>
                            <br/>
                            <span className="mdScreen" style={{ marginTop: '-5.3125rem'}}>Price</span>
                        </Tab>
                    </TabList>
                    <TabPanel>
                        <TabOne />
                    </TabPanel>

                    <TabPanel>
                        <TabTwo />
                    </TabPanel>

                    <TabPanel>
                        <TabThree />
                    </TabPanel>
                </Tabs>
                
            </div>
        )
    }
}

export default TabComponent
